import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    frutas: [
      {nombre: 'Manzana', cantidad: 0},
      {nombre: 'Pera', cantidad: 0},
      {nombre: 'Naranja', cantidad: 0},
    ]
  },
  mutations: {
    aumentar(state, index) {
      state.frutas[index].cantidad ++ 
    },
    reiniciar(state) {
      state.frutas.forEach(elemento => {
        elemento.cantidad = 0
      })
    }
  },
  actions: {
  },
  modules: {
  }
})

/* Se pueden llevar los valores estáticos del state a una "action"
   de esta forma consumir los valores desde una API o un servidor,
   las mutaciones unicamente cambian los valores de un estado

   los estados se llaman en el componente desde la propiedad "computed" debido
   a que se mantiene bajo vigilancia los cambios en el estado, las mutaciones se colocan
   en "methods" debido a que devuelven una función como respuesta */